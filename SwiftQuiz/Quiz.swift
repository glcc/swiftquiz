//
//  Quiz.swift
//  SwiftQuiz
//
//  Created by Gerson  on 05/08/2018.
//  Copyright © 2018 Gerson . All rights reserved.
//

import Foundation

class Quiz {
    let question: String
    let options: [String]
    private let correctedAnswer: String
    
    init(question: String, options: [String], correctedAnswer: String) {
        self.question = question
        self.options = options
        self.correctedAnswer = correctedAnswer
    }
    
    func validadeOption(_ index: Int) -> Bool {
        let answer = options[index]
        return answer == correctedAnswer
    }
    
    deinit {
        print("Quiz removed from memory")
    }
}
